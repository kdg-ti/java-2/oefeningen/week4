package be.kdg.supplier;

import java.util.function.Supplier;


public class MySupplier {
    public Piloot returnPiloot() {
        // Vul hieronder de juiste Lambda expression aan
        // Maak een nieuw Piloot object met als parameters
        // "Torro Rosso", "Verstappen" en 33.
        //
        // Supplier<Piloot> pilootSupplier = ...
        // return pilootSupplier.get();
        return null;
    }

    public String groet(Supplier<String> supplier) {
        // Vul hier aan, vervang "..." door de juiste oproep
        return "Hello " + "..." + "!";
    }


}
