import employee.Data;
import employee.Employee;
import employee.Gender;
import employee.Role;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Vervolledig de volgende code. Maak telkens gebruik van een Stream object op basis van myList
 * De verwachte afdruk staat onderaan
 * */
public class DemoStreams {
    public static void main(String[] args) {
        List<Employee> myList = Data.getDataAsList();

        //Opgave 1:
        //...
        System.out.println("Alle vrouwen van Executive volgens leeftijd:");

        //Opgave 2:
        double result = 0.0;
        //...
        System.out.printf("\nGemiddelde leeftijd van de Sales werknemers: %.1f\n", result);

        //Opgave 3:
        String str = "";
        //...
        System.out.println("\nAlfabetische opsomming van de managers:\n" + str);

        //Opgave 4:
        //...
        long aantal = 0;
        System.out.println("\nHet aantal mannen op de afdeling \"ENG\": " + aantal);
    }
}

/*
Verwachte afdruk:

Alle vrouwen van Executive volgens leeftijd:
Smith Nancy     age: 55 gender: FEMALE   e-mail: phil.smith@example.com         role: EXECUTIVE    dept: HR
Jones Betty     age: 65 gender: FEMALE   e-mail: betty.jones@example.com        role: EXECUTIVE    dept: Sales

Gemiddelde leeftijd van de Sales werknemers: 47,3

Alfabetische opsomming van de managers:
Adams John, Doe John, Johnson James

Het aantal mannen op de afdeling "ENG": 3
 */
