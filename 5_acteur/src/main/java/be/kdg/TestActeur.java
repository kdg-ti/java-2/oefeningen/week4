package be.kdg;

import be.kdg.acteur.Acteur;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Zie tekstbestand voor de opgave.
 */

public class TestActeur {
    private static final Acteur[] testdata = {
            new Acteur("Cameron Diaz", 1972),
            new Acteur("Anna Faris", 1976),
            new Acteur("Angelina Jolie", 1975),
            new Acteur("Jennifer Lopez", 1970),
            new Acteur("Reese Witherspoon", 1976),
            new Acteur("Neve Campbell", 1973),
            new Acteur("Catherine Zeta-Jones", 1969),
            new Acteur("Kirsten Dunst", 1982),
            new Acteur("Kate Winslet", 1975),
            new Acteur("Gina Philips", 1975),
            new Acteur("Shannon Elisabeth", 1973),
            new Acteur("Carmen Electra", 1972),
            new Acteur("Drew Barrymore", 1975),
            new Acteur("Elisabeth Hurley", 1965),
            new Acteur("Tara Reid", 1975),
            new Acteur("Katie Holmes", 1978),
            new Acteur("Anna Faris", 1976)
    };

    public static void main(String[] args) {
        Acteur reese = new Acteur("Reese Witherspoon", 1976);
        Acteur drew = new Acteur("Drew Barrymore", 1975);
        Acteur anna = new Acteur("Anna Faris", 1976);
        Acteur thandie = new Acteur("Thandie Newton", 1972);

        List<Acteur> acteurs = new ArrayList<>();

        acteurs.addAll(Arrays.asList(testdata));
        acteurs.add(reese);
        acteurs.add(drew);
        acteurs.add(anna);
        acteurs.add(thandie);

        // Toon de inhoud van de collection (zonder stream)


        // Verwijder de objecten reese en thandie (reeds uitgewerkt)
        acteurs.remove(reese);
        acteurs.remove(thandie);

        // Verwijder alle acteurs geboren in 1975
        // Verwijder alle acteurs die meer dan éénmaal voorkomen
        // Sorteer volgens geboortejaar (oudste eerst) en dan volgens voornaam
        //
        // acteurs = acteurs.stream()....



        // Toon de inhoud van de collection (zonder stream)


    }
}

/* Verwachte uitvoer:

1972  Cameron Diaz
1976  Anna Faris
1975  Angelina Jolie
1970  Jennifer Lopez
1976  Reese Witherspoon
1973  Neve Campbell
1969  Catherine Zeta-Jones
1982  Kirsten Dunst
1975  Kate Winslet
1975  Gina Philips
1973  Shannon Elisabeth
1972  Carmen Electra
1975  Drew Barrymore
1965  Elisabeth Hurley
1975  Tara Reid
1978  Katie Holmes
1976  Anna Faris
1976  Reese Witherspoon
1975  Drew Barrymore
1976  Anna Faris
1972  Thandie Newton

 Uiteindelijke inhoud:

1965  Elisabeth Hurley
1969  Catherine Zeta-Jones
1970  Jennifer Lopez
1972  Cameron Diaz
1972  Carmen Electra
1973  Neve Campbell
1973  Shannon Elisabeth
1976  Anna Faris
1976  Reese Witherspoon
1978  Katie Holmes
1982  Kirsten Dunst
*/

